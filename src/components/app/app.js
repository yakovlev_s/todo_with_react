import React from 'react'

import AppHeader from '../app-header/app-header'
import SearchPanel from '../search-panel/search-panel'
import TodoList from '../todo-list/todo-list'
import ItemStatusFilter from '../item-status-filter/item-status-filter'

import './app.css'

const App = () => {
  const todoData = [
    { label: 'Drink tea', important: true, id: 1 },
    { label: 'Watch a video', important: false, id: 2 },
    { label: 'Do sth', important: true, id: 3 }
  ]
  const botayed = false
  const botayBox = <h2>Ботай, пожалуйста!</h2>
  return (
    <div>
      <span>{(new Date()).toString() + ' - ВРЕМЯ БОТАТЬ'}</span>
      {botayed ? null : botayBox}
      <AppHeader toDo={1} done={3} />
      <div className="top-panel d-flex">
        <SearchPanel />
        <ItemStatusFilter />
      </div>

      <TodoList 
        todos={todoData}
        onDeleted={(id) => console.log('del', id)}
      />
    </div>
  )
}

export default App